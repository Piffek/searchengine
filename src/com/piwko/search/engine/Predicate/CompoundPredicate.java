package com.piwko.search.engine.Predicate;

import com.piwko.search.engine.Product.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class CompoundPredicate implements Predicate<Product> {
    private Relation relation;
    private List<Predicate<Product>> predicates;

    public CompoundPredicate(Relation relation, List<Predicate<Product>> predicates) {
        this.relation = relation;
        this.predicates = new ArrayList<>(predicates);
    }

    @Override
    public boolean test(Product product) {
        int predicatesArraySize = predicates.size();
        List<Predicate<Product>> correctResults = new ArrayList<>();

        for(Predicate<Product> predicate : predicates) {
            if(predicate.test(product)) {
                correctResults.add(predicate);
            }
        }

        switch (relation) {
            case OR:
                return correctResults.size() <= predicatesArraySize && correctResults.size() != 0;
            case AND:
                return correctResults.size() == predicatesArraySize;
            default:
                throw new UnsupportedOperationException("Unsuported operation: "+ relation.name());
        }
    }

    public enum Relation {
        AND, OR
    }
}
