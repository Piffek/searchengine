package com.piwko.search.engine.Predicate;

import com.piwko.search.engine.Product.Product;

import java.util.Optional;
import java.util.function.Predicate;

public class StringPredicate implements Predicate<Product> {

    private String propertyName;
    private StringOperator operation;
    private String referneceValue;

    public StringPredicate(String propertyName, StringOperator operation, String referneceValue) {
        this.propertyName = propertyName;
        this.operation = operation;
        this.referneceValue = referneceValue;
    }

    @Override
    public boolean test(Product product) {

        Optional<String> property = product.getStringProperty(propertyName);

        switch (operation) {
            case NOT_EQUAL:
                return ! property.get().equals(referneceValue);
            case EQUAL:
                return property.get().equals(referneceValue);
            default:
                throw new  UnsupportedOperationException("Unsuported operation: "+ operation.name());
        }
    }

    public enum StringOperator {
        EQUAL, NOT_EQUAL;
    }
}
