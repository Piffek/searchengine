package com.piwko.search.engine;

import com.piwko.search.engine.Engine.Option;
import com.piwko.search.engine.Engine.SearchEngine;
import com.piwko.search.engine.Predicate.CompoundPredicate;
import com.piwko.search.engine.Predicate.IntegerPredicate;
import com.piwko.search.engine.Predicate.StringPredicate;
import com.piwko.search.engine.Product.Product;
import com.piwko.search.engine.Product.ProductSet;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        Map<String, String> stringProperty = new HashMap<>();
        stringProperty.put("wielosc", "mała");
        Map<String, Integer> integerProperty = new HashMap<>();
        integerProperty.put("cena", 20);

        Product product = new Product(1L, "Pierwszy", stringProperty, integerProperty);

        Map<String, String> stringProperty2 = new HashMap<>();
        stringProperty2.put("wielosc", "duza");
        Map<String, Integer> integerProperty2 = new HashMap<>();
        integerProperty2.put("cena", 10);

        Product product2 = new Product(2L, "Drugi", stringProperty2, integerProperty2);

        Set<Product> availableListOfProduct = new HashSet<>();
        availableListOfProduct.add(product);
        availableListOfProduct.add(product2);
        ProductSet productSet = new ProductSet(availableListOfProduct);

        IntegerPredicate integerPredicate = new IntegerPredicate("cena", IntegerPredicate.IntegerOperator.EQUAL, 30);
        StringPredicate stringPredicate = new StringPredicate("wielosc", StringPredicate.StringOperator.EQUAL, "mała");

        List<Predicate<Product>> predicates = Arrays.asList(integerPredicate, stringPredicate);
        CompoundPredicate compoundPredicate = new CompoundPredicate(CompoundPredicate.Relation.AND, predicates);

        List<Predicate<Product>> compundPredicates = Arrays.asList(compoundPredicate);

        List<Option> options = Collections.singletonList(new Option(1L, compundPredicates));

        SearchEngine searchEngine = new SearchEngine(productSet, options);
        searchEngine.result().forEach(System.out::println);
    }
}
