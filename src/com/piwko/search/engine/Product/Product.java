package com.piwko.search.engine.Product;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public final class Product {
    private final Long id;
    private final String name;
    private final Map<String, String> stringProperties;
    private final Map<String, Integer> integerProperties;

    public Product(Long id, String name, Map<String, String> stringProperties, Map<String, Integer> integerProperties) {
        this.id = id;
        this.name = name;
        this.stringProperties = Collections.unmodifiableMap(stringProperties);
        this.integerProperties = Collections.unmodifiableMap(integerProperties);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Optional<String> getStringProperty(String name) {
        return Optional.ofNullable(stringProperties.get(name));
    }

    public Optional<Integer> getIntegerProperty(String name) {
        return Optional.ofNullable(integerProperties.get(name));
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", stringProperties=" + stringProperties +
                ", integerProperties=" + integerProperties +
                '}';
    }
}
