package com.piwko.search.engine.Product;

import java.util.Set;

public class ProductSet {
    private Set<Product> productSet;

    public ProductSet(Set<Product> productSet) {
        this.productSet = productSet;
    }

    public Set<Product> getProductSet() {
        return productSet;
    }
}
