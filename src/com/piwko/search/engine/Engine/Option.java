package com.piwko.search.engine.Engine;

import com.piwko.search.engine.Product.Product;

import java.util.List;
import java.util.function.Predicate;

public class Option {
    private Long id;
    private List<Predicate<Product>> predicates;

    public Option(Long id, List<Predicate<Product>> predicates) {
        this.id = id;
        this.predicates = predicates;
    }

    public Long getId() {
        return id;
    }

    public List<Predicate<Product>> getPredicates() {
        return predicates;
    }
}
