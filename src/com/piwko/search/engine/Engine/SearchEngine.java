package com.piwko.search.engine.Engine;

import com.piwko.search.engine.Product.Product;
import com.piwko.search.engine.Product.ProductSet;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SearchEngine {
    private ProductSet productSet;
    private List<Option> selectedOption;

    public SearchEngine(ProductSet productSet, List<Option> selectedOption) {
        this.productSet = productSet;
        this.selectedOption = selectedOption;
    }

    public List<Product> result() {
        List<Product> productResult = new ArrayList<>();
        for(Product product : productSet.getProductSet()) {
            for (Option option : selectedOption) {
                for (Predicate<Product> predicate : option.getPredicates()) {
                    if(predicate.test(product)) {
                        productResult.add(product);
                    }
                }
            }
        }
        return productResult;
    }
}
